const { initConnectionsAsync } = require('@fulldive/common/src/connections')

module.exports = async (bishop, config) => {
  // bishop.* contains set of ready-to-use instances, examples:
  // bishop.koa - http rest
  // bishop.router - http rest router
  // bishop.log - logger
  // bishop.tracer - opentracing
  // prometheus - promethes metrics
  const { log } = bishop

  // init connection if they need: mongodb, redis etc...
  const connections = await initConnectionsAsync(config.connections, log)
  bishop.embed('connections', connections)

  // load rest api
  await bishop.use(require('./bishop/api'), config)

  // load own logic
  await bishop.use(require('./bishop/common'), config)

  // load service-specific methods
  await bishop.use(require('./bishop/service'), config)
}
