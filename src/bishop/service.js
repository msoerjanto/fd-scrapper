const { version, name, description } = require('../../package.json')

module.exports = async bishop => {
  const { log } = bishop

  bishop.add('cmd:health', async () => {
    // here we can check service health status
    // unhealthy services will be removed from service mesh till success health check
    return { version, name, description }
  })

  bishop.add('cmd:sigint', async () => {
    // here we can handle graceful user disconnections, etc
    log.info('Caught SIGINT, closing connections...')
  })
}
