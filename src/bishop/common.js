// any business logic we want to write goes here

const DEFAULT_VALUE = 'value is not set'
module.exports = async bishop => {
  const { log, connections } = bishop
  const { test } = connections.mongodb.collections

  bishop.add('role:test, cmd:set, key', async message => {
    const { key, value = DEFAULT_VALUE } = message
    const $set = { value }
    await test.updateOne({ _id: key }, { $set }, { upsert: true })
    log.info('Set "%s" on key "%s"', value, key)
  })

  bishop.add('role:test, cmd:get, key', async message => {
    const { key } = message
    const doc = await test.findOne({ _id: key })
    log.info('Set value from key "%s"', key)
    return doc && doc.value
  })
}
