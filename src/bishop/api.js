// REST API interface
// framework we use: https://www.npmjs.com/package/koa
// routing is performed via: https://www.npmjs.com/package/koa-better-router

module.exports = async bishop => {
  const { router } = bishop

  router.get('/set', async ctx => {
    const { value } = ctx.query
    await bishop.act('role:test, cmd:set, key:example', { value })
    ctx.body = { succes: true }
  })

  router.get('/get', async ctx => {
    const value = await bishop.act('role:test, cmd:get, key:example')
    ctx.body = { success: true, value }
  })
}
