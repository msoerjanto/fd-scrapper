# FullDive nodejs service template
_NOTICE: this is not complete set of documentation and can be changed in further, plese refer to according people for specific implementations :)_

## Service can perform following operations:
- handles incoming HTTP request via REST interface
- communicates with other services using pattern-matching library
- returns prometheus-compatible metrics on `/metrics` route
- reports own health status
- handle SIGINT signals to safely close user connections
- inform execution plans using traces
- able to configure over ENV variable
- is deployable using docker/kubernetes templates

and so on


Currently, this is a preferable way to deploy application into FullDive environment

## Directory structure
- `/bin` - service-specific binaries (launchers, scripts etc.)
- `/config` - default and environment-specific configuration files
- `/deployment` - deployment configuration files
- `/src/bishop` - main service logic performed as bishop plugins, all code goes here
- `/test` - set of integrational tests
- `.drone.yml` - drone CI pipeline - http://docs.drone.io/getting-started/
- `docker-compose.yml` - set of external services for development and testing (if this operations require external services) - https://docs.docker.com/compose/compose-file/compose-file-v2/
- `Dockerfile` - information how to pack service into docker container


## Supported libraries

### Configuration
- https://www.npmjs.com/package/config
### REST API
- https://www.npmjs.com/package/koa
- https://www.npmjs.com/package/koa-better-router
### Monitoring
- https://www.npmjs.com/package/prom-client
- https://www.npmjs.com/package/jaeger-client
- https://www.npmjs.com/package/pino
### Testing
- https://www.npmjs.com/package/mocha

### Available commands
- `yarn start`
- `yarn test`
- `curl http://localhost:8080/test`
