#!/usr/bin/env node

// service launcher
const { initService } = require('@fulldive/bishop-launcher')
const config = require('config')

const service = require('../src')
initService(service, config)
